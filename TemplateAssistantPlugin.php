<?php

namespace Craft;

/**
 * Class TemplateAssistantPlugin
 * @package Craft
 * @author Obermeijer, Nick (n.obermeijer@itmundi.nl)
 */
class TemplateAssistantPlugin extends BasePlugin {

    /**
     * Returns the plugin’s version number.
     *
     * @return string The plugin’s version number.
     */
    public function getVersion()
    {
        return '1.0.2';
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return Craft::t('Itmundi Template Assistant');
    }

    /**
     * Returns the plugin developer’s name.
     *
     * @return string The plugin developer’s name.
     */
    public function getDeveloper()
    {
        return 'Itmundi Development Team';
    }

    /**
     * Returns the plugin developer’s URL.
     *
     * @return string The plugin developer’s URL.
     */
    public function getDeveloperUrl()
    {
        return 'http://www.itmundi.nl/';
    }

}