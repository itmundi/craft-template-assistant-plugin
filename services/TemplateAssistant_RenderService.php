<?php

namespace Craft;

/**
 * Class TemplateAssistant_RenderService
 * @package Craft
 * @author Obermeijer, Nick (n.obermeijer@itmundi.nl)
 */
class TemplateAssistant_RenderService {

    const SITE_TEMPLATE_PATH = 'frontend-templates';


    /**
     * @var null|string
     */
    protected $originalPath = null;


    /**
     * Init which is obviously required for services
     */
    public function init() {
        // Do something?
    }

    /**
     * Simple plugin check
     * @param null|BasePlugin $plugin
     * @return bool
     */
    public function isPlugin($plugin) {
        return ($plugin instanceof BasePlugin);
    }

    /**
     * Returns full path to plugin
     * @param string $plugin
     * @return string
     * @throws Exception
     */
    public function getPluginPath($plugin) {
        $pluginObject = craft()->plugins->getPlugin($plugin);
        if($this->isPlugin($pluginObject)) { // When it's an valid plugin extract its full path
            $pluginReflection = new \ReflectionClass(get_class($pluginObject));
            $pluginFileName = $pluginReflection->getFileName();

            return str_replace(basename($pluginFileName), '', $pluginFileName);
        }
        throw new Exception(sprintf('Plugin "%s" not found', $plugin));
    }

    /**
     * Removes possible forward slash from templatePath string
     * @param string $plugin Plugin name
     * @param string $templatesPath Path to templates
     * @return string
     * @throws Exception
     */
    public function getPluginTemplatePath($plugin, $templatesPath)
    {
        return $this->getPluginPath($plugin) . ltrim($templatesPath, '/');
    }

    /**
     * @param $plugin
     * @param $templatesPath
     */
    public function addLoaderForPlugin($plugin, $templatesPath = self::SITE_TEMPLATE_PATH)
    {
        /** @var \Twig_Environment $twig */
        $twig = craft()->templates->getTwig();

        $pluginTemplatePath = $this->getPluginTemplatePath($plugin, $templatesPath);

        $pluginLoader = $this->getPluginLoader($plugin, $pluginTemplatePath);

        $loaderChain = new \Twig_Loader_Chain(array($twig->getLoader(), $pluginLoader));

        $twig->setLoader($loaderChain);
    }

    /**
     * Restores original path if set
     */
    protected function restoreOriginalPath()
    {
        if(!is_null($this->originalPath)) {
            craft()->path->setTemplatesPath($this->originalPath);
            $this->originalPath = null; // Clear
        }
        throw new Exception('Original template path has not been set');
    }

    /**
     * Returns loader for the plugin
     * @param string $namespace
     * @param string $pluginTemplatePath
     * @return \Twig_Loader_Filesystem
     * @throws \Twig_Error_Loader
     */
    protected function getPluginLoader($namespace, $pluginTemplatePath) {
        $pluginLoader = new \Twig_Loader_Filesystem();
        $pluginLoader->addPath($pluginTemplatePath, $namespace);

        return $pluginLoader;
    }

    /**
     * Renders template from plugin directory
     * @param string $plugin Plugin name
     * @param string $template Path to template to render
     * @param array $context Arguments to pass to template
     * @param string $templatesPath
     * @return string
     * @throws \Twig_Error_Loader
     */
    public function getRenderedPluginTemplate(
        $plugin,
        $template,
        array $context = array(),
        $templatesPath = self::SITE_TEMPLATE_PATH
    ) {
        if (empty($plugin)) {
            $templatePath = $template;
        } else {
            $this->addLoaderForPlugin($plugin, $templatesPath);

            $templatePath = '@' . $plugin . '/' . $template;
        }

        return craft()->templates->render($templatePath, $context);
    }

    /**
     * Renders and displays template
     * @param string $plugin Plugin name
     * @param string $template Path to template to render
     * @param array $args Arguments to pass to template
     * @param string $templatesPath
     */
    public function render($plugin, $template, array $args = array(), $templatesPath = self::SITE_TEMPLATE_PATH) {
        echo $this->getRenderedPluginTemplate($plugin, $template, $args, $templatesPath);

        craft()->end();
    }

}
